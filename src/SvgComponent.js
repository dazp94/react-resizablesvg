import React from "react";

const BackGround = props => (
  <svg {...props} viewBox={`0 0 1737 2058`}>
    <path
      fill="url(#a)"
      d="M129.592 39.527C2.164 117.186-38.15 283.392 39.547 410.758L1104.75 2058h632.14L501.002 129.529C423.305 2.163 257.019-38.132 129.592 39.527z"
    />
    <defs>
      <linearGradient
        id="a"
        x1="3431.98"
        x2="-99.617"
        y1="3478.61"
        y2="317.762"
        gradientUnits="userSpaceOnUse"
      >
        <stop stop-color="#D930A2" />
        <stop offset=".503" stop-color="#6875E2" />
        <stop offset="1" stop-color="#51AFE2" />
      </linearGradient>
    </defs>
  </svg>
);

class SvgComponent extends React.Component {
  state = { width: 0, height: 0 };

  //   componentDidMount() {
  //     this.updateWindowDimensions();
  //     window.addEventListener("resize", this.updateWindowDimensions);
  //   }

  //   componentWillUnmount() {
  //     window.removeEventListener("resize", this.updateWindowDimensions);
  //   }

  //   updateWindowDimensions = () => {
  //     this.setState({ width: window.innerWidth, height: window.innerHeight });
  //   };

  render() {
    return <BackGround />;
  }
}
export default SvgComponent;
